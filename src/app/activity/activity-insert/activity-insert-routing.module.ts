import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AddActivityComponent } from './components/add-activity/add-activity.component';

const routes: Routes = [
  {path:'insert',
  component:AddActivityComponent
}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ActivityInsertRoutingModule { }
